<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendNotiRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'noti'  =>  'required|min:2|max:100',
        ];
    }

    public function messages()
    {
        return [
            'noti.*'  => 'Thông báo phải từ 10 đến 100 ký tự',
        ];
    }
}
